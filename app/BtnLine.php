<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BtnLine extends Model
{
//    protected $dateFormat = 'Y-m-d H:i:s';
    protected $table = '_btblInvoiceLines';
    protected $primaryKey = 'idInvoiceLines';
    protected $connection = 'sqlsrv2';
}
