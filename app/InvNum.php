<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvNum extends Model
{
//    protected $dateFormat = 'Y-m-d H:i:s';
    protected $table = 'InvNum';
    protected $primaryKey = 'AutoIndex';
    protected $connection = 'sqlsrv2';
}
