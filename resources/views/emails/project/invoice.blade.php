@component('mail::message')
# Introduction

{{ ucfirst($data['message']) }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
